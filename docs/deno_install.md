# Installer Deno

Lancer la commande pour installer Deno

```
curl -fsSL https://deno.land/x/install/install.sh | sh
```

Exemple de résultat attendu (penser à suivre les instructions pour finaliser l'installation).
Remarque, ne pas copier celles-ci, mais celles qui seront retournées par le script (le résultat est différent selon l'ordinateur qui lance la commande).

```
######################################################################## 100.0%##O=#  #
######################################################################## 100.0%#-#O=#  #
######################################################################## 100.0%
Archive:  /home/marco/.deno/bin/deno.zip
  inflating: /home/marco/.deno/bin/deno
Deno was installed successfully to /home/marco/.deno/bin/deno
Manually add the directory to your $HOME/.zshrc (or similar)
  export DENO_INSTALL="/home/marco/.deno"
  export PATH="$DENO_INSTALL/bin:$PATH"
Run '/home/marco/.deno/bin/deno --help' to get started
```

**(OPTIONNEL)** S'il manque le paquet "unzip" il faudra l'installer (ex de commande pour Ubuntu ou Debian), puis relancer la commande d'installation de Deno

```
sudo apt-get install unzip

curl -fsSL https://deno.land/x/install/install.sh | sh
```

Enfin, penser à ajouter les lignes suivantes dans le fichier  ".bashrc" ou équivalent.

Relancer le terminal, puis vérifier que la commande deno fonctionne.

```
deno --version
```

Résultat attendu
```
deno 1.18.2 (release, x86_64-unknown-linux-gnu)
v8 9.8.177.6
typescript 4.5.2
```
