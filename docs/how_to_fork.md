
- Aller à la page suivante dans ton navigateur : https://gitlab.com/marc-bouvier/learning-js
- Cliquer sur le bouton "Fork"
    ![capture montrant où se situe le bouton Fork sur un projet Gitlab](assets/02_fork_learning_js.png)
- Nom de projet : Choisir un nom (ou laisser le nom existant)
- Project URL: utiliser ton compte comme espace de nom
- Niveau de visibilté! "public"
    ![Capture montrant un exemple de configuration de dépôt dans github](assets/03_fork_learning_js.png)

